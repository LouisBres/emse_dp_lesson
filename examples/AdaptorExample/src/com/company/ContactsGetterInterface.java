package com.company;

import java.util.AbstractList;

public interface ContactsGetterInterface {
    public AbstractList<Contact> getContacts();
}
