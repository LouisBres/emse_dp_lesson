package com.company;

public abstract class AbstractMessageSender {
    public abstract void sendMessage(String title, String details, int importance);
}

